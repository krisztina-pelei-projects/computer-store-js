let balance = 0;
let pay = 0;
let loan = 0;

//buttons
const getLoanBtn = document.getElementById("get-loan-btn");
const transferToBankBtn = document.getElementById("transfer-to-bank-btn");
const getPayBtn = document.getElementById("get-pay-btn");
const repayLoanBtn = document.getElementById("repay-loan-btn");
const buyBtn = document.getElementById("buy-btn");

//display fields
let displayBalance = document.getElementById("display-balance");
let displayLoan = document.getElementById("display-loan");
let loanContainer = document.getElementById("loan-container");
let displayPay = document.getElementById("display-pay");

let displayLaptopName = document.getElementById("display-laptop-name");
let displayLaptopDescription = document.getElementById("display-laptop-description");
let displayLaptopPrice = document.getElementById("display-laptop-price");

let computerSelect = document.getElementById("computer-select");
let listOfFeatures = document.getElementById("list-of-features");
let laptopImg = document.getElementById("laptop-img");

//onclick functionality
getPayBtn.onclick = function () {
    getPay();
    displayMoneyValues();
}

transferToBankBtn.onclick = function () {
    transferToBank();
    displayMoneyValues();
}

getLoanBtn.onclick = function () {
    getLoan(window.prompt("Requested amount: ", 0));
    displayMoneyValues()
}

repayLoanBtn.onclick = function () {
    repayLoan();
    displayMoneyValues();
}

//calculations
function getPay() {
    pay = pay + 100;
}

function getLoan(amount) {
    if(isNaN(amount)) {
        alert("Sorry, the input you entered is not a number!")
        return;
    }
    if(amount > (balance * 2)) {
        alert("Requested amount is too high");
    } else if(loan > 0 ) {
        alert("Current loan must be payed before requesting new loan");
    } else {
        loan = amount;
    }
}

function transferToBank() {
    let amount = pay;
    if(loan > 0) {
        loan = loan - (amount*0.1);
        balance = balance + (amount * 0.9);
        pay = 0;
    } else {
        balance = balance + amount;
        pay = 0;
    }
}

function repayLoan() {
    let amount = pay;
    if(amount <= loan) {
        loan = loan - amount;
        pay = 0;
    } else {
        balance = balance + (amount - loan);
        loan = 0;
        pay = 0;
    }
}

function displayMoneyValues() {
    displayPay.innerText = new Intl.NumberFormat('hu-HU', { style: 'currency', currency: 'HUF' }).format(pay);
    displayBalance.innerText = new Intl.NumberFormat('hu-HU', { style: 'currency', currency: 'HUF' }).format(balance);
    displayLoan.innerText = new Intl.NumberFormat('hu-HU', { style: 'currency', currency: 'HUF' }).format(loan);
    if(loan > 0) {
        loanContainer.classList.remove("d-none");
        repayLoanBtn.classList.remove("d-none");
    } else {
        loanContainer.classList.add("d-none");
        repayLoanBtn.classList.add("d-none");
    }
}

displayMoneyValues()

//functionality regarding laptop section
let laptops = [];

async function getComputers() {
    const url = "https://noroff-komputer-store-api.herokuapp.com/computers";

    const fetchParams = {
        method: "GET",
        headers: {
            "content-type": "application/json; charset=UTF-8"
        }
    }

    let laptopsJson;

    try {
        laptopsJson = await fetch(url, fetchParams);
    } catch (e) {
        console.log(e);
    }

    try {
        laptops = await laptopsJson.json();
    } catch (e) {
        console.log(e);
    }

    for (let i = 0; i < laptops.length; i++) {
        let newOption = `<option value="${i}" id="option-${i}">${laptops[i].title}</option>` ;
        computerSelect.innerHTML = computerSelect.innerHTML + newOption;
    }

    renderComputerView(0);
}

getComputers();

buyBtn.onclick = function () {
    let price = parseInt(displayLaptopPrice.innerText);
    buyLaptop(price)
}

function buyLaptop(price) {
    if(price > balance) {
        alert("No sufficient funds to buy this product");
    } else {
        balance = balance - price;
        displayMoneyValues();
        alert("You are now the owner of the new laptop");
    }
}

function renderComputerView(index) {
    let laptop = laptops[index];
    let features = laptop.specs;

    listOfFeatures.innerHTML = "";
    for (let i = 0; i < features.length; i++) {
        let newFeature = `<li>${features[i]}</li>`
        listOfFeatures.innerHTML = listOfFeatures.innerHTML + newFeature;
    }

    displayLaptopName.innerText = laptop.title;
    displayLaptopDescription.innerText = laptop.description;
    displayLaptopPrice.innerText = laptop.price;
    laptopImg.setAttribute("src", `https://noroff-komputer-store-api.herokuapp.com/${laptop.image}`)

}

computerSelect.onchange = function (event) {
    renderComputerView(event.target.value);
}



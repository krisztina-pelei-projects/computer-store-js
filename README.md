# Computer Store

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

**Dynamic Webpage** with **JavaScript**.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Background

This project is a basic website demonstrating concepts of event handling, DOM manipulation and fetching and dynamically rendering data from a restful API. The layout is styled using **Bootstrap 5**.

## Install

- Clone repository

## Usage

- Open index.html in browser

## Maintainer

- [Krisztina Pelei](https://gitlab.com/kokriszti)
